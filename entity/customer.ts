export interface Customer {
    _id?: string
    code:string
    description:string
    addresses?:Address[]
    email?:string
    creationDate:Date
    updateDate:Date
}




export const INITIAL_LIST : Customer[] = [
    {
        code:"A001",
        description: "Apple inc.",
        creationDate: new Date(),
        updateDate: new Date()
    },
    {
        code:"A002",
        description: "Google inc.",
        creationDate: new Date(),
        updateDate: new Date(),
    },
    {
        code:"A003",
        description: "Facebook inc.",
        creationDate: new Date(),
        updateDate: new Date()
    }




]


export interface Address {
	street?: string;
	city?: string;
	province?: string;
	country?: string;
	postalCode?: number;
}

