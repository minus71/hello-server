import { error } from 'console';
import e = require('express');
import NeDB = require('nedb');
import { from, Observable, Subject } from 'rxjs';
import { WSS } from '..';
import { Customer, INITIAL_LIST } from '../entity/customer';

console.log("Loaded DB Instance")

let dbCustomers:Nedb = new NeDB("./customers.db");


dbCustomers.loadDatabase(err=>{
    if(err){
        console.error("Error loading customers db",err);
    } else {
        console.info("Customers DB loaded")
    }
});

function initCustomers(){
    dbCustomers.insert(INITIAL_LIST);
}

export function getCustomers():Customer[]{
   console.log(dbCustomers)
    let data = dbCustomers.getAllData();
    if(!data || data.length==0){
        initCustomers();
        return dbCustomers.getAllData();
    }
    return data;
}

export async function getByCode(code:string): Promise<Customer>{
    return new Promise((resolve,_error)=>dbCustomers.findOne({code},(err,doc)=>{
        if(!err){
            resolve(doc);
        }else {
            _error(err);
        }
    }));
}
export async function getById(_id:string): Promise<Customer>{
    return new Promise((resolve,_error)=>dbCustomers.findOne({_id},(err,doc)=>{
        if(!err){
            resolve(doc);
        }else {
            _error(err);
        }
    }));
}

export async function insert(customer:Customer):Promise<Customer>{
    return new Promise<Customer>(async(resolve,fail)=>{
        if(customer._id){
            fail("Customer already present");
        }else {
            const c2 = await getByCode(customer.code);
            if(c2){
                fail("Customer already present");
            }else {
                try{
                    const newCustomer = await _insert(customer);
                    resolve(newCustomer);
                } catch (e){
                    fail(e);
                }
            }
        }

    });
}

export async function update(customer:Customer){
    validateCustomer(customer);
    if( customer._id){
        const id:string = customer._id;
        const prev = await getById(id);
        const creationDate = prev.creationDate;
        const updateDate = new Date();
        const updated : Customer= {...customer,creationDate:creationDate,updateDate};
        return _update(updated);
    } else {
        throw new Error("Can't update customer");
    }

}
async function _update(customer:Customer){
    return new Promise<Customer>((resolve,fail)=>{
        if(customer._id){
            const _id = customer._id;
            dbCustomers.update({_id},customer,{},(err)=>{
                if(err){
                    fail(new Error("Failed to update customer: "+err.message));
                }else {
                    const data = { context: "Customer", op: "update", _id };
                    notify(_id,data);
    
                    resolve(customer);
                }
            });
        }
    })
}

export async function deleteCustomer(id:any) {
    return _delete(id);
}
async function _delete(id:any){
    return new Promise<Customer>((resolve,fail)=>{
        dbCustomers.remove({_id:id},{},(err)=>{
            if(err){
                fail(new Error("Failed to update customer: "+err.message));
            }else {
                const data = { context: "Customer", op: "delete", id };
                notify(id,data);
                resolve(id);
            }
        });
    })
}


function notify(id: any, data:any) {
    WSS.clients.forEach(ws => {
        const reply = JSON.stringify(data);
        ws.send(reply);
    });
}

async function _insert(customer:Customer):Promise<Customer>{
    validateCustomer(customer);
    return new Promise(async (resolve,fail)=>{
        fixCustomer(customer);
        dbCustomers.insert(customer,(err,doc)=>{
            if(err){
                fail(Error("DB error "+err));
            }else {
                resolve(doc);
            }
        });
    
    })
}




function fixCustomer(customer: Customer) {
    customer.creationDate = new Date();
    if (customer._id) {
        delete customer._id;
    }
}

export function validateCustomer(customer: Customer) {
    if(customer.code==null){
        throw new Error("Customer must have a code");
    } else if(customer.description==null){
        throw new Error("Customer must have a description");
    } 
}

