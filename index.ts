import express, {  json, NextFunction,  Request,  Response,  Router } from 'express';
import expressWs from 'express-ws';
import {CorsOptions} from 'cors';
import cors from 'cors';



// import * as bodyParser from 'body-parser';
import customerRouter from './routes/customer-routes';
// rest of the code remains same
const _app = express();
const PORT = 8000;

const errorHandler= (err: Error, req: Request, res: Response, next: NextFunction)=>{
  res.status(500).send({error:"Method failed: "+err.message});
}



const router = Router();

const base = expressWs(_app);
const app =base.app;
app.use(cors());
app.use("/",router);
app.use(express.json({type:"*/*"}));
app.use(express.urlencoded({extended:true}));
app.use(errorHandler);

router.get("/",(req, res) => res.send({greeting:"Hello from express"}));

router.use('/customer',customerRouter)



app.ws("/events",(ws,req,next)=>{
  console.log("XXX")
  ws.on("message", async (msg)=>{
    const data = JSON.parse(<string>msg);
    console.log(data)
  });
});

export const WSS = base.getWss(); 
console.log(WSS.path);
console.log(WSS.options);

console.log(WSS)

app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`); 
});
