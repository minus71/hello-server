import { Router, json, raw, urlencoded } from 'express';
import { deleteCustomer, getByCode, getById, getCustomers, insert,  update } from '../service/customer-store';

const customerRouter = Router();

customerRouter.get('', (request, response) => {
  response.send(getCustomers());
});
customerRouter.use(json({type:"*/*"}));
customerRouter.use(urlencoded({extended:true}));


customerRouter.get('/code/:code', async (request, response) => {
  const {code} = request.params;
  let customer =  await getByCode(code);
  response.send(customer);
});

customerRouter.get('/:id', async (request, response) => {
  const {id} = request.params;
  let customer =  await getById(id);
  response.send(customer);
});

customerRouter.delete('/:id', async (request, response) => {
  const {id} = request.params;
  try{
    let res =  await deleteCustomer(id);
    response.send({id:res});
  } catch(e){
    response.status(400).send({error:e.message})
  }
});

customerRouter.post('/',async (request, response) => {
  
  console.log("POST",request.body)
  try{

    if(request.body._id || request.body._id==''){
      delete request.body._id;
    }

    const updated = await insert(request.body);
    response.send(updated);
  } catch(e){
    const msg = e.message ? e.message : e;
    response.status(400).send({error:msg})
  }
});

customerRouter.patch('/',async (req,resp) => {
  const {body} = req;
  try{
    const updated = await update(body);
    resp.send(updated);
  } catch (e){
    resp.status(400).send({error:e.message});
  }
})


export default customerRouter;
